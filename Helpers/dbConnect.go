package Helpers

import (
	"fmt"
	"os"
	"sprint3/Models"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"
	"github.com/subosito/gotenv"
)

func DbConnect() *gorm.DB {
	e := gotenv.Load()
	if e != nil {
		fmt.Print(e)
	}
	user := os.Getenv("user")
	password := os.Getenv("pass")
	dbname := os.Getenv("name")
	host := os.Getenv("host")
	port := os.Getenv("port")
	dbUri := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	fmt.Println(dbUri)
	db, err := gorm.Open("postgres", dbUri)

	if err != nil {
		panic(err.Error())
	}

	return db

}

func Migration() {
	db := DbConnect()
	db.AutoMigrate(&Models.Driver{})

}
