package Routers

import (
	"net/http"
	Controllers "sprint3/Controllers"

	"github.com/gorilla/mux"
)

func InitRouter() *mux.Router {
	mux := mux.NewRouter()
	// Driver router
	mux.HandleFunc("/late", Controllers.Alert_DriverLate).Methods("GET")
	mux.HandleFunc("/noy", Controllers.NotwDriver).Methods("GET")
	mux.HandleFunc("/no", Controllers.NoDriver).Methods("GET")
	mux.HandleFunc("/nodelivery", Controllers.CancelDelivery).Methods("GET")
	mux.HandleFunc("/noclient", Controllers.CancelClientRide).Methods("GET")
	mux.HandleFunc("/noride", Controllers.CancelRide).Methods("GET")
	mux.HandleFunc("/yesdelivery", Controllers.ConfirmDelivery).Methods("GET")
	mux.HandleFunc("/yesrd", Controllers.ConfirmClientRide).Methods("GET")
	mux.HandleFunc("/okride", Controllers.ConfirmRide).Methods("GET")
	mux.HandleFunc("/invoice", Controllers.DriverInvoice).Methods("GET")
	mux.HandleFunc("/tmpl", Controllers.ReadCurrentDir).Methods("GET")
	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		w.Write([]byte("Welcome, to Driver section ."))
	})

	// Customer router

	mux.HandleFunc("/latecustomer", Controllers.Alert_CustomerLate).Methods("GET")
	mux.HandleFunc("/nocustomer", Controllers.NoCustomer).Methods("GET")
	mux.HandleFunc("/nodeliveryc", Controllers.CancelDeliveryCustomer).Methods("GET")
	mux.HandleFunc("/nod", Controllers.CancelDriverRide).Methods("GET")
	mux.HandleFunc("/noridec", Controllers.CustomerCancelRide).Methods("GET")
	mux.HandleFunc("/yesdelic", Controllers.CustomerConfirmDelivery).Methods("GET")
	mux.HandleFunc("/okridec", Controllers.ConfirmDriverRide).Methods("GET")
	mux.HandleFunc("/Cinvoice", Controllers.CustomerInvoice).Methods("GET")
	mux.HandleFunc("/tmplc", Controllers.CreadCurrentDir).Methods("GET")

	return mux

}
